@extends('layouts.app')

@section('title', 'Create new Task')

@section('content')
    <h1>Create new Task</h1>
    <form action="{{ route('web.tasks.store') }}" method="post">
        @include('tasks._form')
    </form>
@endsection
