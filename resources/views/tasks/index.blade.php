@extends('layouts.app')

@section('title', 'All Tasks')

@section('content')
    <h1>Tasks</h1>
    <a href="{{ route('web.tasks.create') }}" class="btn btn-primary">Add New Task</a>
    <ul>
        @foreach($tasks as $task)
            <li class="m-3">
                {{ $task->title }} |
                @include('components.form.buttons.delete', ['url' => route('web.tasks.destroy', $task)])
            </li>
        @endforeach
    </ul>
@endsection
