@csrf
<div class="form-group">
    <label for="title">Title</label>
    <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}">
    @error('title')
    <p class="alert alert-danger">{{ $message }}</p>
    @enderror
</div>
<br>
<div class="form-group">
    <label for="is_done">is done</label>
    <input type="checkbox" name="is_done" id="is_done" value="1" {{old('is_done')? 'checked' : ''}}>
    @error('is_done')
    <p class="alert alert-danger">{{ $message }}</p>
    @enderror
</div>
<br>
<div class="form-group">
    <label for="user_id">User</label>
    <select name="user_id" id="user_id" class="form-control">
        <option value>select user</option>
        @foreach($users as $user)
            <option value="{{$user->id}}" {{old('user_id') == $user->id ? 'selected' : ''}}>
                {{$user->name}}
            </option>
        @endforeach
    </select>
    @error('user_id')
    <p class="alert alert-danger">{{ $message }}</p>
    @enderror
</div>
<br>
<div class="form-group">
    <label for="description">description</label>
    <textarea name="description" id="description" class="form-control" cols="30"
              rows="10">{{old('description')}}</textarea>
    @error('is_done')
    <p class="alert alert-danger">{{ $message }}</p>
    @enderror
</div>

<div class="text-center mt-5">
    <button class="btn btn-success">Save</button>
</div>
