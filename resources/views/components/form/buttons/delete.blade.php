<form style="display: inline" action="{{ $url }}" method="post">
    @method('DELETE')
    @csrf
    <button class="btn btn-danger">Delete</button>
</form>
