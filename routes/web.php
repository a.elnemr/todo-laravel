<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'as' => 'web.tasks.',
    'prefix' => 'tasks',
    'middleware' => 'auth'
], function () {
    Route::get('/', 'TaskController@index')->name('index');
    Route::get('/create', 'TaskController@create')->name('create');
//    Route::view('/create', 'tasks.create')->name('create'); // if return only view
    Route::post('/', 'TaskController@store')->name('store');
    Route::delete('/{task}', 'TaskController@destroy')->name('destroy');
});

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
