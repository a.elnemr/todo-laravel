<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/**
 * 0 - Request
 * 1 - Route
 * -- Middleware
 * 2 - Controller
 * 3 - Database
 * -- Middleware
 * 4 - Response (blade, json)
 */


//Route::namespace('App\Http\Controllers\API')->prefix('1.0')->group(function () {
Route::group([
    'namespace' => 'App\Http\Controllers\API', // Namespace controllers prefix
    'prefix' => '1.0', // URI prefix
    'as' => 'api.v1.', // Route name prefix
], function () {
    Route::post('/login', 'Auth\LoginController@issueToken')->name('login');

    Route::middleware(['auth:sanctum'])->group(function () {
        Route::as('tasks.')->group(function () {
            Route::get('/tasks', 'TaskController@index')->name('index');
            Route::post('/tasks', 'TaskController@store')->name('store');
        });
        Route::apiResource('users', 'UserController')->except(['destroy']);
    });

});
