<?php

namespace App\Http\Controllers\API;

use AElnemr\RestFullResponse\CoreJsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\TaskRequest;
use App\Http\Resources\TaskListResource;
use App\Http\Resources\TaskShowResource;
use App\Repository\TaskRepositoryInterface;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;

    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function index(Request $request)
    {
        $allTasks = $this->taskRepository->paginate(5, $request);
        return $this->okWithPagination(TaskListResource::collection($allTasks));
    }


    public function store(TaskRequest $request)
    {
//        $this->auth($request);
        $task = $this->taskRepository->create($request);
        return $this->created((new TaskShowResource($task))->resolve());
    }

    // we should use middleware
//    public function auth(Request $request)
//    {
//        if ($request->query('token') !== '12345') {
//            return $this->unauthorized();
//        }
//    }
}
