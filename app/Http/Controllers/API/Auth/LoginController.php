<?php


namespace App\Http\Controllers\API\Auth;


use AElnemr\RestFullResponse\CoreJsonResponse;
use App\Http\Controllers\Controller;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Nyholm\Psr7\Response as Psr7Response;
use Psr\Http\Message\ServerRequestInterface;

class LoginController extends AccessTokenController
{
    use CoreJsonResponse;

    /**
     * Authorize a client to access the user's account.
     *
     * @param  \Psr\Http\Message\ServerRequestInterface  $request
     * @return \Illuminate\Http\Response
     */
    public function issueToken(ServerRequestInterface $request)
    {
        return $this->withErrorHandling(function () use ($request) {
            $response = $this->convertResponse(
                $this->server->respondToAccessTokenRequest($request, new Psr7Response)
            );

            return $this->ok(json_decode($response->content(), 1));
        });
    }
}
