<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Models\Task;
use App\Repository\Eloquent\TaskRepository;
use App\Repository\TaskRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(TaskRepositoryInterface $taskRepository, UserRepositoryInterface $userRepository)
    {
        $this->taskRepository = $taskRepository;
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        return view('tasks.index', [
            'tasks' => $this->taskRepository->paginate(5, $request)
        ]);
    }

    public function create()
    {
        $users = $this->userRepository->limit(5)->get();
//        return view('tasks.create', compact('users'));
        return view('tasks.create')->with('users', $users);
    }

    public function store(TaskRequest $request)
    {
        $this->taskRepository->create($request);
        return redirect(route('web.tasks.index'));
    }

    public function destroy(Task $task)
    {
        $task->delete();
        return redirect(route('web.tasks.index'));
    }
}
