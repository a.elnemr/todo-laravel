<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->user? (new UserResource($this->user)) : null;
        return [
            'id' => $this->id,
            'title' => $this->title,
            'user' => $user,
            'creator' => $this->creator
        ];
    }
}
