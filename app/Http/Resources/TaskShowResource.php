<?php

namespace App\Http\Resources;

use AElnemr\RestFullResponse\Helper\EmptyData;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $creator = $this->creator? (new UserResource($this->creator)) : EmptyData::object();
        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->description,
            'is_done' => (boolean)$this->is_done,
            'user' => $this->user,
            'creator' => $creator,
        ];
    }
}
