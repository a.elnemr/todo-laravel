<?php

namespace App\Http\Middleware;

use AElnemr\RestFullResponse\CoreJsonResponse;
use Closure;
use Illuminate\Http\Request;

class AuthUser
{
    use CoreJsonResponse;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->query('token') !== '12345') {
            return $this->unauthorized();
        }

        return $next($request);
    }
}
