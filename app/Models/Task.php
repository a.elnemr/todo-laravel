<?php

namespace App\Models;

use App\Http\Resources\UserResource;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'is_done',
        'user_id',
        'created_by'
    ];

//    protected $append = [
//        'user'
//    ];

    protected $hidden = [
        'created_by',
        'user_id',
    ];

    protected $casts = [
        'is_done' => 'boolean',
        'created_at' => 'timestamp'
    ];

    public function user()
    {
//        return $this->belongsTo('App\Models\User', 'user_id');
        return $this->belongsTo(User::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::create($this->attributes['created_at'])->timestamp;
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::create($this->attributes['updated_at'])->timestamp;
    }


//    public function getUserAttribute()
//    {
//        return $this->user()->first();
//    }
    public function toArray()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->description,
            'user' => $this->user? new UserResource($this->user) : null
        ];
    }

//    public function toArray()
//    {
//        return [
//            'id' => $this->id,
//            'title' => $this->title,
//            'body' => $this->description,
//            'user' => $this->user/*? [
//                'id' => $this->user->id,
//                'name' => $this->user->name,
//            ] : null,*/
//        ];
//    }
}
