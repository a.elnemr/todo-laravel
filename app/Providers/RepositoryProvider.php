<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repository\TaskRepositoryInterface',
            'App\Repository\Eloquent\TaskRepository'
        );

        $this->app->bind(
            'App\Repository\UserRepositoryInterface',
            'App\Repository\Eloquent\UserRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
