<?php


namespace App\Repository;


use App\Models\Task;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

interface TaskRepositoryInterface extends BaseRepositoryInterface
{
    public function filter(Request $request): Builder;

    public function get(?Request $request);
}
