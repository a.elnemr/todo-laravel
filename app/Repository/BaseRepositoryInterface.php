<?php


namespace App\Repository;


use App\Models\Task;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface BaseRepositoryInterface
{
    public function create(Request $request);

    public function all();

    public function paginate(int $limit = 10, Request $request);
}
