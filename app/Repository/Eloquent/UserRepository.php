<?php


namespace App\Repository\Eloquent;


use App\Models\Task;
use App\Models\User;
use App\Repository\TaskRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

//    public function create(Request $request)
//    {
//        $data = $request->all();
//        $data['password'] = Hash::make($data['password']);
//        return $this->model::query()->create($data);
//    }

}
