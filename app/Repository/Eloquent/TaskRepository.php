<?php

namespace App\Repository\Eloquent;

use App\Models\Task;
use App\Repository\TaskRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class TaskRepository extends BaseRepository implements TaskRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    public function __construct(Task $model)
    {
        $this->model = $model;
    }

    public function filter(Request $request): Builder
    {
        return $this->model::query()
            ->when($request->query->has('is_done'), function (Builder $builder) use ($request) {
                $builder->where('is_done', $request->query('is_done'));
            })
            ->when($request->query->has('user_id'), function (Builder $builder) use ($request){
                $builder->where('user_id', $request->query('user_id'));
            })
            ->when($request->query->has('created_by'), function (Builder $builder) use ($request){
                $builder->where('created_by', $request->query('created_by'));
            });
    }

    public function get(?Request $request)
    {
        if ($request === null) {
            return $this->model::query()->get();
        }

        return $this->filter($request)->get();
    }

    /**
     * @param int $limit
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function paginate(int $limit = 10, Request $request)
    {
        if ($request === null) {
            return $this->model::query()->latest()->paginate($limit);
        }

        return $this->filter($request)->latest()->paginate($limit);
    }
}
