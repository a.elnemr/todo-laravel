<?php


namespace App\Repository\Eloquent;


use App\Models\Task;
use App\Repository\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class BaseRepository implements BaseRepositoryInterface
{
    public function create(Request $request)
    {
        return $this->model::query()->create($request->all());
    }

    public function all()
    {
        return $this->model::query()->get();
    }

    public function paginate(int $limit = 10, Request $request)
    {
        return $this->model::query()->latest()->paginate($limit);
    }

    public function __call($name, $arguments)
    {
        return $this->model->$name(... $arguments);
    }
}
